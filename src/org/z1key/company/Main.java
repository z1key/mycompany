package org.z1key.company;

import org.z1key.company.entities.*;
import org.z1key.company.interfaces.Worker;

import java.util.LinkedHashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Set<Class<? extends Worker>> profs = new LinkedHashSet<>();
        profs.add(DirectorImpl.class);
        profs.add(ManagerImpl.class);
        profs.add(AccountantImpl.class);
        profs.add(ProgrammerImpl.class);
        profs.add(ViewDesignerImpl.class);
        profs.add(TesterImpl.class);
        Company company = new Company("MyCompany", profs);
        company.startCareer();
    }
}
