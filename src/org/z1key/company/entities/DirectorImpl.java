package org.z1key.company.entities;

import org.z1key.company.Properties;
import org.z1key.company.interfaces.Director;
import org.z1key.company.interfaces.Manager;
import org.z1key.company.util.Helper;
import org.z1key.company.util.MessageBuilder;
import org.z1key.company.util.Randomizer;

/**
 * Created by User on 22.02.2017.
 */
public class DirectorImpl extends AbstractWorker implements Director {

    {
        System.out.println(TYPE + ": \"Hi guys! I am your BOSS! I will find job for you!\"\n");
    }
    public static final String TYPE = "Director";
    public DirectorImpl(Company company) {
        this(company, 2500F);
    }
    DirectorImpl(Company company, float customSalary) {
        super(company);
        salary = customSalary;
    }

    @Override
    public void startCareer() {
        Thread thread = new Thread(() -> {
           while (true) {
               giveWork();
               try {
                   int time = Helper.getTimeOf(1F/Randomizer.getRandomBetween(1,2));
                   Thread.sleep(time);
               } catch (InterruptedException e) {
                   //
               }
           }
        });
        thread.setName(TYPE);
        thread.start();
    }

    @Override
    public void giveWork() {
        Company company = getCompany();

        Work.Type type = Work.Type.values()[Randomizer.getRandomOf(Work.Type.values().length)];
        int size = Randomizer.getRandomBetween(type.min, type.max);
        Work work = new Work(type, size);
        System.out.println("! ! ! NEW WORK CAME IN!\n" + TYPE + ": \"" + work.getType().directorReply + ". \nYou have " + work.getSize() + " hours to finish it\"");

        company.addWork(work);

        Manager manager = company.getManager();
        manager.doThis(work);
    }

    @Override
    public void doThis(Work work) {
        throw new UnsupportedOperationException("Nobody can say to Director what he must do!");
    }

    @Override
    public String getType() {
        return TYPE;
    }
}
