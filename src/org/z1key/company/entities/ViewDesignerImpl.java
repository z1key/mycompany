package org.z1key.company.entities;

import org.z1key.company.interfaces.ViewDesigner;

public class ViewDesignerImpl extends OfficeWorker implements ViewDesigner {
    private static int idRepository = 1;
    public static final String TYPE = "Designer";
    public ViewDesignerImpl(Company company) {
        this(company, 10F);
    }
    ViewDesignerImpl(Company company, float customSalary) {
        super(company, customSalary);
        this.id = idRepository++;
        System.out.println(TYPE + " " + id + ": \"Hi! I am Front-end designer! There is beautiful weather out the window!\"\n");
    }

    @Override
    public String getType() {
        return TYPE;
    }

    @Override
    public void design(Task task) {
        setTask(task);
        doThis(this.task.getWork());
    }
}
