package org.z1key.company.entities;

import org.z1key.company.interfaces.Freelancer;
import org.z1key.company.interfaces.Manager;
import org.z1key.company.interfaces.ViewDesigner;
import org.z1key.company.util.Helper;

/**
 * Created by User on 22.02.2017.
 */
public class FreelancerDesigner implements Freelancer, ViewDesigner {
    public static final String TYPE = "Freelancer(Designer)";

    private Manager manager;
    private Task task;
    private float salaryPerHour = 20.0F;
    public FreelancerDesigner(Manager manager) {
        this.manager = manager;
        System.out.println(TYPE + ": Hi! Are you finding free worker? I am here and ready to start!");
    }

    public void setTask(Task task) {
        this.task = task;
        task.setWorker(this);
    }

    @Override
    public void doThis(Work work) {
        Thread thread = new Thread(() -> {
            try {
                System.out.println(String.format("%s: \"Got Task #%d [Time = %d]. Processing of Work #%d [%s] has begun!\"",TYPE, task.getId(), task.getTime(), task.getWork().getId(), task.getWork().getType().toString()));
                Thread.sleep(Helper.getTimeOf(task.getTime()));
            } catch (InterruptedException e) {
                //
            }
            System.err.println("Freelancer(Designer): \"Task" + task.getWork().getType().toString() +", "+ task.getId() + " done!\"");
            manager.closeTask(task);
        });
        thread.setName(TYPE);
        thread.start();
    }

    @Override
    public Company getCompany() {
        return null;
    }

    @Override
    public float getSalary() {
        return salaryPerHour;
    }

    @Override
    public String getType() {
        return TYPE;
    }

    @Override
    public void design(Task task) {
        setTask(task);
        doThis(this.task.getWork());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FreelancerDesigner)) return false;

        FreelancerDesigner that = (FreelancerDesigner) o;

        return Float.compare(that.salaryPerHour, salaryPerHour) == 0;

    }

    @Override
    public int hashCode() {
        return (salaryPerHour != +0.0f ? Float.floatToIntBits(salaryPerHour) : 0);
    }

}
