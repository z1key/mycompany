package org.z1key.company.entities;

import org.z1key.company.Properties;
import org.z1key.company.interfaces.*;
import org.z1key.company.util.Helper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

public class AccountantImpl extends AbstractWorker implements Accountant {

    private float companyBudget = 0F;
    private int hour = 1;
    private int day = 1;

    {
        System.out.println(TYPE + ": \"Hi, colleges! I am Accountant! I will count your salary every week!\"\n");
    }

    public static final String TYPE = "Accountant";

    public AccountantImpl(Company company) {
        this(company, 1000F);
    }

    AccountantImpl(Company company, float customSalary) {
        super(company);
        salary = customSalary;
    }

    @Override
    public void doThis(Work work) {

    }

    @Override
    public String getType() {
        return TYPE;
    }

    @Override
    public void audit(final Company company) {
        System.out.println(TYPE + ": \"A U D I T\"");
        Report report = new Report(company);
        report.print();
        report.export();
    }

    @Override
    public void startCareer() {
        Thread thread = new Thread(() -> {
            while (true) {
                try {
                    if (hour > 8) {
                        hour = 1;
                        day++;
                    }
                    System.out.println("\n\nDay:" + day + " Hour:" + hour);
                    if (day != 1 && (day - 1) % 5 == 0 && hour == 1) {
                        resetRestrictions();
                    }
                    if (day != 1 && (day - 1) % 10 == 0 && hour == 1) {
                        audit(getCompany());
                    }
                    Thread.sleep(Helper.getTimeOf(1));
                    hour++;
                } catch (InterruptedException e) {
                    //
                }
            }
        });
        thread.setName(TYPE);
        thread.start();
    }

    void resetRestrictions() {
        Map<Class<? extends Worker>,List<Worker>> staff = getCompany().getStaff();
        for (Class clazz : staff.keySet()) {
            List<Worker> workers = staff.get(clazz);
            for (Worker worker : workers) {
                if (worker instanceof OfficeWorker) {
                    ((OfficeWorker)worker).hoursAvailable = ((OfficeWorker)worker).getHourLimitPerWeek();
                }
            }
        }
    }
    class Report{

        Company company;

        float balanceBefore = 0F;
        float balanceAfter = 0F;

        float debit = 0F;
        float credit = 0F;

        private List<Work> works;
        private List<Task> tasks;

        int worksDone = 0;
        int worksTotal = 0;
        int tasksTotal = 0;
        int tasksDone = 0;

        String document;

        Map<Worker, Map<String, Number>> workerMap;

        Report(Company company) {

            this.company = company;
            this.balanceBefore = companyBudget;
            this.works = new ArrayList<>(company.getWorks());
            company.getWorks().removeAll(works);
            this.tasks = new ArrayList<>(company.getManager().getTasks());
            company.getManager().getTasks().removeAll(tasks);
            workerMap = new LinkedHashMap<>();
            prepare();
            create();
        }

        private void prepare() {
            for (Work work : works) {
                worksTotal++;
                if (work.isClosed()) {
                    debit += work.getSize() * Properties.COMPANY_PRICE;
                    worksDone++;
                }
            }
            companyBudget += debit;
            balanceAfter = companyBudget;

            Director director = getCompany().getDirector();
            Map<String, Number> directorInfo = new HashMap<>();
            directorInfo.put("salary", director.getSalary());
            directorInfo.put("tasks", 0);
            directorInfo.put("hours", 80);
            directorInfo.put("total", director.getSalary());
            workerMap.put(director, directorInfo);
            credit += director.getSalary();

            Map<String, Number> accountantInfo = new HashMap<>();
            accountantInfo.put("salary", AccountantImpl.this.getSalary());
            accountantInfo.put("tasks", 0);
            accountantInfo.put("hours", 80);
            accountantInfo.put("total", AccountantImpl.this.getSalary());
            workerMap.put(AccountantImpl.this, accountantInfo);
            credit += AccountantImpl.this.getSalary();

            Map<String, Number> managerInfo = new HashMap<>();
            Manager manager = getCompany().getManager();
            managerInfo.put("salary", manager.getSalary());
            managerInfo.put("tasks", 0);
            managerInfo.put("hours", 80);
            managerInfo.put("total", manager.getSalary());
            workerMap.put(manager, managerInfo);
            credit += manager.getSalary();

            Iterator<Task> taskIterator = tasks.iterator();
            while (taskIterator.hasNext()) {
                Task task = taskIterator.next();
                tasksTotal++;
                if (task.isClosed()) {
                    tasksDone++;
                    credit += task.getTime() * task.getCost();
                    Worker worker = task.getWorker();
                    Map<String, Number> workerInfo = workerMap.get(worker);
                    if (workerInfo == null) {
                        workerInfo = new HashMap<>();
                        workerInfo.put("salary", worker.getSalary());
                        workerInfo.put("tasks", 1);
                        workerInfo.put("hours", task.getTime());
                        workerInfo.put("total", task.getTime() * task.getCost());
                        workerMap.put(worker, workerInfo);
                    } else {
                        Integer i = (Integer) workerInfo.get("tasks");
                        workerInfo.put("tasks", i+1);
                        i = (Integer) workerInfo.get("hours");
                        workerInfo.put("hours", i + task.getTime());
                        Float f = (Float) workerInfo.get("total");
                        workerInfo.put("total", f + task.getTime() * task.getCost());
                    }
                    taskIterator.remove();
                }
            }
            companyBudget -= credit;
        }

        void create() {
            StringBuilder sb = new StringBuilder("Report for \"" + company.getTitle() + "\":\n")
            .append(String.format("%s: Current balance: $%.2f\n",TYPE, balanceBefore))
            .append(String.format("%s: Works total: %d, Works done: %d. Got: $%.2f. Total balance: $%.2f\n",TYPE, worksTotal, worksDone, debit, balanceAfter))
            .append(String.format("%s: Task total: %d, Tasks done: %d. Paid: $%.2f. Total balance: $%.2f\n",TYPE, tasksTotal, tasksDone, credit, companyBudget))
            .append("========================================================================================\n");
            for (Worker worker : workerMap.keySet()) {
                Map<String,Number> wInfo = workerMap.get(worker);
                if (worker instanceof OfficeWorker) {
                    OfficeWorker w = (OfficeWorker)worker;
                    sb.append(String.format("%-25s%d: Hours: %d\tTasks done: %d\tSalary per hour: $%.2f\tTotal earned: $%.2f\n", w.getType(), w.getId(), (int)wInfo.get("hours"), (int)wInfo.get("tasks"), w.getSalary(), (float)wInfo.get("total")));
                } else if (worker instanceof Freelancer){
                    sb.append(String.format("%-25s: \tHours: %d\tTasks done: %d\tSalary per hour: $%.2f\tTotal earned: $%.2f\n", worker.getType(), (int)wInfo.get("hours"), (int)wInfo.get("tasks"), worker.getSalary(), (float)wInfo.get("total")));
                } else {
                    sb.append(String.format("%-25s: \tFixed salary paid: $%.2f\n", worker.getType(), (float)wInfo.get("total")));
                }
            }
            sb.append("========================================================================================\n");
            document = sb.toString();
        }
        void print() {
            System.out.println(document);
        }
        void export() {
            if (document == null) return;
            File file = new File("report[day-" + day + "].txt");
            FileOutputStream fileOutputStream = null;
            PrintWriter pw = null;
            try {
                fileOutputStream = new FileOutputStream(file);
                pw = new PrintWriter(fileOutputStream);
                pw.write(document);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    pw.close();
                    fileOutputStream.close();
                } catch (Exception ex) {
                    //
                }
            }
        }
    }
}
