package org.z1key.company.entities;

import org.z1key.company.interfaces.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ManagerImpl extends AbstractWorker implements Manager {
    {
        System.out.println(TYPE + ": \"Ok! Listen to me, everyone! I am your BOSS of the BOSS! Understand? Move your ass!!!\"\n");
    }

    public static final String TYPE = "Manager";
    private final List<Task> tasks;

    public ManagerImpl(Company company) {
        this(company, 1500F);
    }

    ManagerImpl(Company company, float customSalary) {
        super(company);
        salary = customSalary;
        this.tasks = Collections.synchronizedList(new ArrayList<>());
    }

    @Override
    public void doThis(final Work work) {
        final WorkManage workManage = WorkManage.valueOf(work.getType());
        if (workManage == null)
            throw new RuntimeException(TYPE + ": \"Do not know how manage this process - " + work.getType().toString() + ".\"");
        Thread thread = new Thread(() -> {
            System.out.println(TYPE + ": \"" + work.getType().toString() + " received. Will be done, Sir!\"");
            workManage.start(this, work);
        });
        thread.setName(TYPE);
        thread.start();
    }

    @Override
    public synchronized Task createTask(Work work, int hours) {
        Task task = new Task(work, hours);
        this.tasks.add(task);
        return task;
    }

    @Override
    public synchronized void closeTask(Task task) {
        System.err.println("Manager: Task #" + task.getId() + " closed.");
        task.close();
        Work work = task.getWork();
        if (work.isTested()) {
            work.close();
            return;
        }
        if (checkWorkReadyForTest(work)) {
            System.err.println("= Manager: Work #" + work.getId() + " done. Moving to test.");
            moveToTest(work);
        }
    }

    @Override
    public String getType() {
        return TYPE;
    }

    @Override
    public List<Task> getTasks() {
        return tasks;
    }

    private boolean checkWorkReadyForTest(final Work work) {
        boolean isReady = true;
        for (Task task : work.getTasks()) {
            if (!task.isClosed()) {
                isReady = false;
                break;
            }
        }
        return isReady;
    }

    private synchronized void moveToTest(Work work) {
        int testingHours = work.getSize();
        for (Task task : work.getTasks()) {
            testingHours -= task.getTime();
        }
        Tester tester = null;
        while (tester == null) {
            Company company = this.getCompany();
            tester = company.getFreeWorker(Tester.class, testingHours);
            if (tester == null) {
                System.out.println(TYPE + ": \"[" + work.getType().toString() + "] No testers free now. Wait for tester gets free...\"");
                try {
                    this.wait();
                } catch (InterruptedException e) {
                    //
                }
            }
        }
        Task task3 = createTask(work, testingHours);
        tester.test(task3);
    }

    enum WorkManage {
        CREATE_WEBSITE(Work.Type.CREATE_WEBSITE) {
            synchronized void start(Manager manager, Work work) {
                System.out.println(TYPE + ": \"Designer, Programmer come to me, I have work for you!\"");
                int designingHours = (int) (0.4F * work.getSize());
                int programmingHours = (int) (0.4F * work.getSize());

                ViewDesigner designer = manager.getCompany().getFreeWorker(ViewDesigner.class, designingHours);
                if (designer == null) {
                    System.out.println(TYPE + ": \"No designers free now. Calling freelancer.\"");
                    designer = new FreelancerDesigner(manager);
                }
                Task task1 = manager.createTask(work, designingHours);
                work.getTasks().add(task1);
                designer.design(task1);

                Programmer programmer = manager.getCompany().getFreeWorker(Programmer.class, programmingHours);
                if (programmer == null) {
                    System.out.println(TYPE + ": \"No programmers free now. Calling freelancer.\"");
                    programmer = new FreelancerProgrammer(manager);
                }
                Task task2 = manager.createTask(work, programmingHours);
                work.getTasks().add(task2);
                programmer.program(task2);
            }
        },
        DEBUG_CODE(Work.Type.DEBUG_CODE) {
            synchronized void start(Manager manager, Work work) {
                System.out.println(TYPE + ": \"Programmer, come to me, I have work for you!\"");
                int programmingHours = (int) (0.7F * work.getSize());

                Programmer programmer = manager.getCompany().getFreeWorker(Programmer.class, programmingHours);
                if (programmer == null) programmer = new FreelancerProgrammer(manager);
                Task task = manager.createTask(work, programmingHours);
                work.getTasks().add(task);
                programmer.program(task);
            }
        },
        ADD_NEW_FEATURE(Work.Type.ADD_NEW_FEATURE) {
            synchronized void start(Manager manager, Work work) {
                System.out.println(TYPE + ": \"Programmer come to me, I have work for you!\"");
                int programmingHours = (int) (0.7F * work.getSize());

                Programmer programmer = manager.getCompany().getFreeWorker(Programmer.class, programmingHours);
                if (programmer == null) programmer = new FreelancerProgrammer(manager);
                Task task = manager.createTask(work, programmingHours);
                work.getTasks().add(task);
                programmer.program(task);
            }
        },
        CHANGE_DESIGN(Work.Type.CHANGE_DESIGN) {
            synchronized void start(Manager manager, Work work) {
                System.out.println(TYPE + ": \"Designer, come to me, I have work for you!\"");
                int designingHours = (int) (0.9F * work.getSize());

                ViewDesigner designer = manager.getCompany().getFreeWorker(ViewDesigner.class, designingHours);
                if (designer == null) {
                    System.out.println(TYPE + ": \"No designers free now. Calling freelancer.\"");
                    designer = new FreelancerDesigner(manager);
                }
                Task task = manager.createTask(work, designingHours);
                work.getTasks().add(task);
                designer.design(task);
            }
        },
        FIX_CRITICAL_ERROR(Work.Type.FIX_CRITICAL_ERROR) {
            synchronized void start(Manager manager, Work work) {
                System.out.println(TYPE + ": \"Programmer, come to me, I have work for you!\"");
                int programmingHours = (int) (0.9F * work.getSize());

                Programmer programmer = manager.getCompany().getFreeWorker(Programmer.class, programmingHours);
                if (programmer == null) {
                    System.out.println(TYPE + ": \"No programmers free now. Calling freelancer.\"");
                    programmer = new FreelancerProgrammer(manager);
                }
                Task task = manager.createTask(work, programmingHours);
                work.getTasks().add(task);
                programmer.program(task);
            }
        };
        private final Work.Type value;

        WorkManage(Work.Type workType) {
            value = workType;
        }

        public static WorkManage valueOf(Work.Type workType) {
            for (WorkManage e : WorkManage.values()) {
                if (e.value == workType) {
                    return e;
                }
            }
            return null;
        }

        abstract void start(Manager manager, Work work);
    }
}
