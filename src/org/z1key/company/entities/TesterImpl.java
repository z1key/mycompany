package org.z1key.company.entities;

import org.z1key.company.interfaces.Tester;

public class TesterImpl extends OfficeWorker implements Tester {
    private static int idRepository = 1;
    public static final String TYPE = "Tester";
    public TesterImpl(Company company) {
        this(company, 10F);
    }
    TesterImpl(Company company, float customSalary) {
        super(company, customSalary);
        this.id = idRepository++;
        System.out.println(TYPE + " " + id + ": \"Hi! I am Tester! Who will be my next victim?\"\n");
    }

    @Override
    public synchronized void doThis(Work work) {
        super.doThis(work);
        work.setTested(true);
    }

    @Override
    public String getType() {
        return TYPE;
    }

    @Override
    public void test(Task task) {
        setTask(task);
        doThis(this.task.getWork());
    }
}
