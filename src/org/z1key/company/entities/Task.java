package org.z1key.company.entities;

import org.z1key.company.interfaces.Worker;

public class Task {
    private static volatile long idRepository = 1;

    private Work work;
    private long id;
    private int time;
    private float cost;
    private Worker worker;
    private boolean isClosed = false;

    public Task(Work work, int time) {
        this.id = idRepository++;
        this.work = work;
        this.time = time;
    }

    public Work getWork() {
        return work;
    }

    public long getId() {
        return id;
    }

    public int getTime() {
        return time;
    }

    public Worker getWorker() {
        return worker;
    }

    public boolean isClosed() {
        return isClosed;
    }

    public void close() {
        isClosed = true;
    }

    public float getCost() {
        return cost;
    }

    public void setWorker(Worker worker) {
        this.worker = worker;
        this.cost = worker.getSalary();
    }
}
