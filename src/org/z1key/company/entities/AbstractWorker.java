package org.z1key.company.entities;

import org.z1key.company.interfaces.Worker;

public abstract class AbstractWorker implements Worker {
    protected boolean isBusy = false;
    private Company company;
    protected float salary = 0F;

    AbstractWorker(Company company) {
        super();
        this.company = company;
    }

    @Override
    public Company getCompany() {
        return company;
    }
    public abstract String getType();

    @Override
    public float getSalary() {
        return salary;
    }

    public boolean isBusy() {
        return isBusy;
    }
}
