package org.z1key.company.entities;

import java.util.HashSet;
import java.util.Set;

public class Work {

    private static volatile long workIdRepository = 1;

    private Type type;
    private int size;
    private long id;
    private Set<Task> tasks;
    private boolean isTested = false;
    private boolean isClosed = false;

    public Work(Type type, int size) {
        this.type = type;
        this.size = size;
        this.tasks = new HashSet<>();
        this.id   = workIdRepository++;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public long getId() {
        return id;
    }

    public Set<Task> getTasks() {
        return tasks;
    }

    public boolean isTested() {
        return isTested;
    }

    public void setTested(boolean tested) {
        isTested = tested;
    }

    public boolean isClosed() {
        return isClosed;
    }

    public void close() {
        isClosed = true;
        System.err.println("== Work #" + getId() + " is closed.");
    }

    enum Type {

        CREATE_WEBSITE("Create web-site", 40, 80),
        DEBUG_CODE("Find bugs is the code", 4, 8),
        ADD_NEW_FEATURE("Add some program feature", 16, 32),
        CHANGE_DESIGN("Change design of page", 8, 16),
        FIX_CRITICAL_ERROR("COME ON, MAN! FIX THIS IMMEDIATELY! OUR CUSTOMER IS VERY ANGRY! GO GO GO!", 2, 4);

        public final String directorReply;
        public final int min;
        public final int max;

        Type(String directorReply, int minTime, int maxTime) {
            this.directorReply = directorReply;
            this.min = minTime;
            this.max = maxTime;
        }
    }
}
