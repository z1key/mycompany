package org.z1key.company.entities;

import org.z1key.company.Properties;
import org.z1key.company.interfaces.Accountant;
import org.z1key.company.interfaces.Director;
import org.z1key.company.interfaces.Manager;
import org.z1key.company.interfaces.Worker;
import org.z1key.company.util.MessageBuilder;
import org.z1key.company.util.Randomizer;

import java.lang.reflect.Constructor;
import java.util.*;

/**
 * Created by User on 22.02.2017.
 */
public class Company {

    private String title;

    private Director director;
    private Accountant accountant;
    private Manager manager;

    private int size;

    private final List<Work> works;

    private Map<Class<? extends Worker>,List<Worker>> staff;

    public Company(String title, Set<Class<? extends Worker>> professions) {
        if (!validateProfessionList(professions)) throw new RuntimeException("Required professions are missed.");
        size = Randomizer.getRandomBetween(10, Properties.MAX_COWORKERS);
        System.out.println(MessageBuilder.buildMessage("Company \"" + title + "\" has been founded. There will be " + size + " co-workers.\nOpening vacancies..."));
        setStaff(size, professions);
        this.title = title;
        works = Collections.synchronizedList(new ArrayList<>());
    }

    public String getTitle() {
        return title;
    }

    @SuppressWarnings("unchecked")
    synchronized <T extends Worker> T getFreeWorker(Class<T> target, int time) {

        List<Worker> targetList = null;
        for(Iterator<Class<? extends Worker>> i = staff.keySet().iterator(); i.hasNext();) {
            Class clazz = i.next();
            if (target.isAssignableFrom(clazz)) {
                targetList = staff.get(clazz);
            }
        }
        if (targetList != null) {
            for (Iterator<Worker> i= targetList.iterator(); i.hasNext();) {
                Worker w = i.next();
                if (target.isAssignableFrom(w.getClass()) && !((AbstractWorker)w).isBusy()) {
                    if (w instanceof OfficeWorker && ((OfficeWorker) w).getHoursAvailable() >= time)
                    return (T)w;
                }
            }
        }

        return null;
    }
    private boolean validateProfessionList(Set<Class<? extends Worker>> professions) {
        Class<Director> d = Director.class;
        Class<Manager> m = Manager.class;
        Class<Accountant> a = Accountant.class;

        boolean directorFound = false;
        boolean accountantFound = false;
        boolean managerFound = false;

        for(Class p: professions) {
            if (!directorFound && d.isAssignableFrom(p)) {
                directorFound = true;
            } else if (!managerFound && m.isAssignableFrom(p)){
                managerFound = true;
            } else if (!accountantFound && a.isAssignableFrom(p)){
                accountantFound = true;
            }
        }
        return accountantFound && managerFound && directorFound;
    }
    private void setStaff(int size, Set<Class<? extends Worker>> professions) {
        staff = Collections.synchronizedMap(new LinkedHashMap<>());
        Worker worker;
        List<Class<? extends Worker>> profs = new ArrayList<>(professions);
        for (Iterator<Class<? extends Worker>> iter = profs.iterator(); iter.hasNext();) {
            Class<? extends Worker> clazz = iter.next();
            try {
                Constructor<? extends Worker> constructor = clazz.getConstructor(Company.class);
                worker = constructor.newInstance(this);
            } catch (Exception e) {
                throw new RuntimeException("Can't create object of - " + clazz.getName(), e);
            }
            final Worker worker1 = worker;
            staff.put(clazz, new ArrayList<Worker>(){{add(worker1);}});
            if (worker instanceof Director || worker instanceof Accountant || worker instanceof Manager) {
                if (worker instanceof Director) {
                    this.director = (Director) worker;
                }
                if (worker instanceof Accountant) {
                    this.accountant = (Accountant) worker;
                }
                if (worker instanceof Manager) {
                    this.manager = (Manager) worker;
                }
                iter.remove();
            }
            size--;
        }
        while (size > 0) {
            Class<? extends Worker> clazz = profs.get(Randomizer.getRandomOf(profs.size()));
            try {
                Constructor<? extends Worker> constructor = clazz.getConstructor(this.getClass());
                worker = constructor.newInstance(this);
            } catch (Exception e) {
                throw new RuntimeException("Can't create object of - " + clazz.getName(), e);
            }
            staff.get(clazz).add(worker);
            size--;
        }
        professionStatistic();
    }
    private void professionStatistic() {
        StringBuilder stringBuilder = new StringBuilder("Company has next specialists:\n");
        for (Class<? extends Worker> key : staff.keySet()) {
            List<Worker> workers = staff.get(key);
            String type = "";
            try {
                type = (String) key.getField("TYPE").get(null);
            } catch (ReflectiveOperationException e) {
                //
            }
            stringBuilder.append(type).append(": ").append(workers.size()).append(";\n");
        }
        System.out.println(MessageBuilder.buildMessage(stringBuilder.toString()));
    }
    public void startCareer() {
        if (director != null && accountant != null) {
            accountant.startCareer();
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            director.startCareer();
        }
    }
    public Manager getManager() {
        return manager;
    }

    public Director getDirector() {
        return director;
    }

    public Map<Class<? extends Worker>, List<Worker>> getStaff() {
        return staff;
    }

    public void addWork(Work work) {
        works.add(work);
    }
    List<Work> getWorks() {
        return works;
    }
}
