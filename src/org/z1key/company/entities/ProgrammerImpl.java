package org.z1key.company.entities;

import org.z1key.company.interfaces.Programmer;

public class ProgrammerImpl extends OfficeWorker implements Programmer {
    private static int idRepository = 1;
    public static final String TYPE = "Programmer";
    public ProgrammerImpl(Company company) {
        this(company, 15F);
    }
    ProgrammerImpl(Company company, float customSalary) {
        super(company, customSalary);
        this.id = idRepository++;
        System.out.println(TYPE + " " + id + ": \"Hi! I am Java Dev! Bring me coffee, please\"\n");
    }

    @Override
    public String getType() {
        return TYPE;
    }

    @Override
    public void program(Task task) {
        setTask(task);
        doThis(this.task.getWork());
    }
}
