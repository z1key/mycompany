package org.z1key.company.entities;

import org.z1key.company.interfaces.Manager;
import org.z1key.company.util.Helper;

public abstract class OfficeWorker extends AbstractWorker {

    protected int id;
    protected Task task;
    protected Manager manager;
    protected float salaryPerHour = 0F;
    protected int hourLimitPerWeek = 40;
    protected int hoursAvailable = hourLimitPerWeek;

    public OfficeWorker(Company company, float customSalary) {
        super(company);
        setSalaryPerHour(customSalary);
        manager = company.getManager();
    }

    public float getSalary() {
        return salaryPerHour;
    }

    public void setSalaryPerHour(float salaryPerHour) {
        this.salaryPerHour = salaryPerHour;
    }

    public int getHoursAvailable() {
        return hoursAvailable;
    }

    public int getHourLimitPerWeek() {
        return hourLimitPerWeek;
    }

    public Task getTask() {
        return task;
    }

    public int getId() {
        return id;
    }

    public float getSalaryPerHour() {
        return salaryPerHour;
    }

    public synchronized void setTask(Task task) {
        this.task = task;
        task.setWorker(this);
    }

    @Override
    public synchronized void doThis(Work work) {
        isBusy = true;
        System.out.println(String.format("%s %d [%d hours avail.]: \"Got Task #%d [Time = %d]. Processing of Work #%d [%s] has begun!\"",getType(), id, getHoursAvailable(), task.getId(), task.getTime(), task.getWork().getId(), task.getWork().getType().toString()));
        hoursAvailable -= task.getTime();
        Thread thread = new Thread(() -> {
            try {
                Thread.sleep(Helper.getTimeOf(task.getTime()));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.err.println(String.format("%s %d: \"Task #%d [%s] done!", getType(), id,task.getId(), task.getWork().getType().toString()));
            synchronized (manager) {
                manager.notify();
            }
            manager.closeTask(task);
            isBusy = false;
        });
        thread.setName(getType() + id);
        thread.start();
    }
}
