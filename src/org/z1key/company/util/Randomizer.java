package org.z1key.company.util;

import java.util.Random;

/**
 * Created by User on 22.02.2017.
 */
public class Randomizer {

    public static int getRandomOf(int maxValue) {
        Random r = new Random();
        return r.nextInt(maxValue);
    }
    public static int getRandomBetween(int min, int max) {
        Random r = new Random();
        return r.nextInt(max - min + 1) + min;
    }
}
