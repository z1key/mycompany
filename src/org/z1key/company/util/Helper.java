package org.z1key.company.util;

import org.z1key.company.Properties;

/**
 * Created by User on 23.02.2017.
 */
public class Helper {
    public static int getTimeOf(float hours) {
        return (int) (hours * 3600 * 1000 / Properties.TIME_SCALE);
    }
}

