package org.z1key.company;

/**
 * Created by User on 23.02.2017.
 */
public interface Properties {
    int TIME_SCALE = 1000;
    int MAX_COWORKERS = 100;
    int COMPANY_PRICE = 25; // per hour
}
