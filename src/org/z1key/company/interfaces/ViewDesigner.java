package org.z1key.company.interfaces;

import org.z1key.company.entities.Task;

/**
 * Created by User on 22.02.2017.
 */
public interface ViewDesigner extends Worker {
    void design(Task task);
}
