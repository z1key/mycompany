package org.z1key.company.interfaces;

import org.z1key.company.entities.Company;

/**
 * Created by User on 22.02.2017.
 */
public interface Accountant extends Worker {

    void audit(Company company);

    void startCareer();
}
