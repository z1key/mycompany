package org.z1key.company.interfaces;

import org.z1key.company.entities.Task;

/**
 * Created by User on 22.02.2017.
 */
public interface Tester extends Worker {
    void test(Task task);
}
