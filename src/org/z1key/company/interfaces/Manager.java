package org.z1key.company.interfaces;

import org.z1key.company.entities.Task;
import org.z1key.company.entities.Work;

import java.util.List;

/**
 * Created by User on 22.02.2017.
 */
public interface Manager extends Worker {
    Task createTask(Work work,int hours);
    void closeTask(Task task);
    List<Task> getTasks();
}
