package org.z1key.company.interfaces;

import org.z1key.company.entities.Company;
import org.z1key.company.entities.Work;

/**
 * Created by User on 22.02.2017.
 */
public interface Worker {
    void doThis(Work work);
    Company getCompany();
    float getSalary();
    String getType();
}
