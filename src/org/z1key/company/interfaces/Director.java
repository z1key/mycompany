package org.z1key.company.interfaces;

/**
 * Created by User on 22.02.2017.
 */
public interface Director extends Worker {

    void giveWork();

    void startCareer();
}
